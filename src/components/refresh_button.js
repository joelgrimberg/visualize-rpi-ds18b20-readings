import { useRouter } from 'next/router'

function RefreshButton() {
  const router = useRouter()
  const forceReload = () => {
    router.reload()
  }

  return <button onClick={forceReload}>Refresh</button>
}

export default RefreshButton
