import Head from 'next/head'
import * as React from 'react'

function Header() {
  React.useEffect(() => {
    setTimeout(() => {
      window.location.reload()
    }, 10000)
  }, [])

  return (
    <div>
      <Head>
        <title>epic raspberry 🥧</title>
      </Head>
    </div>
  )
}

export default Header
