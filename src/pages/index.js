import file from '../../public/temp.txt'
import readTemperature from '../utils/readTemperature'
import RefreshButton from '../components/refresh_button'
import Header from '../components/head'

var x = readTemperature(file)

const Index = () => (
  <div>
    <Header />
    <h1>🌡️ {x} 🤖</h1>
    <h2>{RefreshButton()}</h2>
  </div>
)

export default Index
