const readTemperature = require('./readTemperature.js')
const loadFixture = require('./loadFixture.js')

const fdr = loadFixture('temp.txt')

test('returns temp', () => {
  expect(readTemperature(fdr)).toBe(18.725)
})
